#
def return_current_month_year_pair():
    return (datetime.date.today().month, datetime.date.today().year)

def return_month_year_pair(transactions):
    return list(set((transaction.date) for transaction in transactions))
