import csv
import transaction_class as tc, credit_card_tools as ct

if __name__ == '__main__':
    with open('../data/test-data.csv', 'r') as csv_file:
        rows = csv.reader(csv_file, delimiter=',')
        transactions = [tc.Transaction(id, row) for id, row in enumerate(rows)
                if id != 0]
    csv_file.close()

    print(ct.sum_unpaid_credit_card_trasactions(transactions))
    transactions = ct.change_credit_card_transactions_to_paid(transactions)
    print(ct.sum_unpaid_credit_card_trasactions(transactions))

    new_transactions = [list(vars(transaction).values())[1:] for transaction in
            transactions]
    with open('../data/test-data-2.csv', 'w') as new_csv_file:
        writer = csv.writer(new_csv_file)
        writer.writerows(new_transactions)
    new_csv_file.close()
