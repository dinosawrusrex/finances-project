import openpyxl
import datetime
import sqlite3
import re

'''
for sheet in current_spendings:
    database_connection.execute_many(
        "INSERT INTO table
        VALUES (? ...)",
        transaction_parameter_iterator)
    convert_date(sheet)
    assign_purpose(sheet)
    export_sheet(sheet)

date_conversion:
    - types: already in iso, dd-mmm-yyyy, dd mmm yyyy, dd MMM yyyy, dd
'''

def main():
    existing_workbook = openpyxl.load_workbook('/home/ds12/Dropbox/spendings.xlsx')
    database_location = './transactions.db'
    #create_database(database_location)

def create_database(database_location):
    database = sqlite3.connect(database_location)
    cursor = database.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS transactions(
            id integer PRIMARY KEY,
            date TEXT,
            payment TEXT,
            amount REAL,
            method TEXT
            credit_paid BOOL,
            purpose TEXT,
            comment TEXT);''')
    database.commit()
    database.close()

def migrate_workbook_into_database(database_location):
    database = sqlite3.connect(database_location)
    cursor = database.cursor()


def compile_transaction_parameter(workbook, row):
    return (workbook.cell(row=row, column=column).value for column in range(1,
    8))

def convert_date(cell, sheet_name, list_option=0):
    date_type = ['%d-%b-%Y', '%d %b %Y', '%d %B %Y']
    list_option = list_option
    if isinstance(cell, str):
        try:
            date = datetime.datetime.strptime(cell, date_type[list_option])
            return datetime.date.isoformat(date)
        except ValueError:
            list_option += 1
            return convert_date(cell, list_option)
    elif isinstance(cell, int):
        month = re.findall('(\D+)\d+', sheet_name)[0]
        year = re.findall('\D+(\d+)', sheet_name)[0]
        date = ' '.join([str(cell), month, year])
        return datetime.date.isoformat(
            datetime.datetime.strptime(date, '%d %b %y'))
    elif isinstance(cell, datetime.datetime):
        return datetime.date.isoformat(cell)

def convert_credit_paid_to_bool(cell):
    if cell == 'Y':
        return True
    elif cell == 'N':
        return False
    return '-'


if __name__ == "__main__":
    main()
