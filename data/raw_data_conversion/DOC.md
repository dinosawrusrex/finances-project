# Raw Data Converter Documentation

## Intro

The current layout of the spending spreadsheet is not very consistent which
makes handling the transaction data a little cumbersome. Firstly, the
spreadsheet contains multiple sheets and I would like to consolidate them to
just one sheet. Secondly, there are other (in this case) useless data that will
get accounted for during the analysis and I would like to remove those values
too.

## Order of Goals

[] Turn dates into iso date formats.
[] Assign 'purpose' to each transaction if not already present.
[] Copy only the columns containing transaction data into a new worksheet, from
each sheet of current spending spreadsheet.
[] Consider using a database; will be easy to put data into database once all
transactions are in one sheet.
