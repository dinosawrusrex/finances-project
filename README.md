# Finances Project

## Introduction

This is an exploratory project that is both for improving my programming
abilities and analysing my finances. I currently manage my transactions through
spreadsheets, but thought about doing this with a program I built instead. This
project will be a nice way to explore project design _e.g._ multiple files,
packages and modules, use files to store information (perhaps writing
information into a csv file), try out Python data analytic tools, and later on,
to create a GUI to use this program!
